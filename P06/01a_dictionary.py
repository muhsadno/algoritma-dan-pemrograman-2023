print('Contoh 1') #menambah/update anggota dictionary
data = {'NIM': 'H071239999', 'ipk': 3.87}
print(data)
data['kampus'] = 'Unhas'
print(data)
data.update({'prodi': 'sistem informasi'})
print(data)
data['NIM'] = 'H071230000'
print(data)

print('\nContoh 2 clear()')
data = {'NIM': 'H071239999', 'ipk': 3.87}
print(data)
data.clear()
print(data)

print('\nContoh 3')
data = {'NIM': 'H071239999', 'ipk': 3.87}
print(data)
print(data.get('NIM'))

print('\nContoh 4')
data = {'NIM': 'H071239999', 'ipk': 3.87, 'prodi': 'sistem informasi'}
print(data)
print(data.keys())
print(list(data.keys())) #convert ke list

print('\nContoh 5')
data = {'NIM': 'H071239999', 'ipk': 3.87, 'prodi': 'sistem informasi'}
print(data)
print(data.values())

print('\nContoh 6')
data = {'NIM': 'H071239999', 'ipk': 3.87, 'prodi': 'sistem informasi'}
print(data)
data.pop('ipk')
print(data)