#cetak bilangan asli < n selain kelipatan 3
n = 16
data = [x for x in range(1,n) if x%3 != 0]
print(data)

# -----------
print()
fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
print(fruits)
newlist = [x if x != "banana" else "orange" for x in fruits] 
print(newlist)