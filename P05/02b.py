'''
keyword argument harus diletakkan setelah positional argument
'''

def func1(arg1=True, arg2): #error
    pass

def func2(arg1, arg2=2023):
    pass

def haloKampus(kampus, nama):
    print(f"Halo saya {nama}, berasal dari {kampus}")

haloKampus(kampus='Unhas', 'Iman') #error