# https://www.hackerrank.com/challenges/simple-array-sum/problem?isFullScreen=true
_ = int(input())
data = map(int, input().split())
print(sum(data))