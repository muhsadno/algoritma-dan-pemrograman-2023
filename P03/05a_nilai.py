n = float(input("Nilai = ")) # n = nilai

#asumsikan nilai yang diinput bisa negatif atau > 100
if n<0 or n>100:
    print('Error!!! Input nilai >= 0 atau <= 100')
else:
    if n>=85 and n<=100:
        print('Nilai huruf A')
    elif n>=80 and n<85:
        print('Nilai huruf A-')
    elif n>=75 and n<80:
        print('Nilai huruf B+')
    elif n>=70 and n<75:
        print('Nilai huruf B')
    elif n>=65 and n<70:
        print('Nilai huruf B-')
    elif n>=60 and n<65:
        print('Nilai huruf C+')
    elif n>=50 and n<60:
        print('Nilai huruf C')
    elif n>=40 and n<50:
        print('Nilai huruf D')
    else:
        print("Nilai huruf E")