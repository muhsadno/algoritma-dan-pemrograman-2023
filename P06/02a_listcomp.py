#list berisi n bilangan kuadrat pertama > 0
n = 4
kuadrat = []
for x in range(1,n+1):
    kuadrat.append(x**2)
print(kuadrat)

#dengan menggunakan list comprehension
kuadratv2 = [x**2 for x in range(1,n+1)]
print(kuadratv2)

