def f(x):
    
    def sqr(u):
        return u**2
    
    return sqr(x) + 2*x + 1

print(f(-1))
#print(sqr(2)) #error, 
# function sqr tidak bisa dipanggil diluar blok utama function f