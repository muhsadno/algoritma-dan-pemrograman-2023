#membuat parameter default
def haloKampus(nama, kampus='Unhas'): 
    print(f"Halo saya {nama}, berasal dari {kampus}")

haloKampus('Iman') #Halo saya Iman, berasal dari Unhas
haloKampus('Junaid', 'UMI') #Halo saya Junaid, berasal dari UMI