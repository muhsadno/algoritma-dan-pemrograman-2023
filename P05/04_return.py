def halo(nama):
    if nama == 'roma':
        return '-- Halo Roma'
    print(f'Kaukah {nama} ?')
    if nama == 'ani':
        return '-- Halo Ani'

# pesan = halo('roma')
# print(pesan)

pesan = halo('ani')
print(pesan)