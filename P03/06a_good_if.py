#asumsikan nilai yang diinput dalam range 0 <= x <= 100
x = float(input())

if x>=75 and x<=100:
    print("A")
elif x>=50 and x<75:
    print("B")
elif x>=25 and x<50:
    print("C")
else:
    print("D")

# if x>=75:
#     print("A")
# elif x>=50:
#     print("B")
# elif x>=25:
#     print("C")
# else:
#     print("D")