# https://tlx.toki.id/problems/bnpchs-2023-penyisihan/A
def sol(s):
    n = len(s)
    res = ''
    if '*' not in s:
        return s
    for i in range(n):
        if s[i] != '*':
            # print(s[i])
            left = s[:i]
            right = s[i+1:]
            cleft = left.count('*')
            cright = right.count('*')
            # print(cleft, cright)
            if cleft%2 == 0 and cright%2 == 0:
                res += s[i]
            else:
                continue
        else:
            continue
    return res


_ = input()
s = input()
print(sol(s))