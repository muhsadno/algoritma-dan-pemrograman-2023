#list dapat menyimpan beberapa tipe data sekaligus
print("Contoh 1")
A = [1, 'a', 0.03, True]
print(f'A = {A}')

#list juga dapat menyimpan list lain 
print("\nContoh 2")
B = [1, 3, 5, [7, 8]]
print(f'B = {B}')

print("\nContoh 3")
C = [] #empty list
print(f'C = {C}')