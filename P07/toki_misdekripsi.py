# https://tlx.toki.id/problems/bnpchs-2023-penyisihan/A
N = int(input())
S = input()

pesan = ''

def hitungBintang(x):
    jum = 0
    for s in x:
        if s == '*':
            jum +=1
        else:
            continue
    return jum

for i in range(N):
    if S[i] == '*':
        continue
    else:
        #pisahkan kiri dan kanan
        kiri = S[:i]
        kanan = S[i+1:]
        #hitung * di kiri dan kanan
        bkiri = hitungBintang(kiri)
        bkanan = hitungBintang(kanan)
        if bkiri%2 == 0 and bkanan%2 == 0:
            pesan += S[i]
     
print(pesan)                