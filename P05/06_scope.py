x = 100 #var. global

def fun1(c):
    y = 1 #var. local
    return x+1+c

def fun2():
    print(2023 + y) #error

def fun():
    y = 23
    def fun3():
        return y+x
    return 2000 + fun3()

u = fun()
print(u) #2123
print(fun1(100)) #201