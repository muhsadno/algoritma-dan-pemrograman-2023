# https://www.hackerrank.com/challenges/diagonal-difference/problem?isFullScreen=true
#versi 1
n = int(input())
matrix = []
for _ in range(n):
    x = list(map(int, input().split()))
    matrix.append(x)

dutama = [matrix[i][i] for i in range(n)]
dsamp = [matrix[i][n-i-1] for i in range(n)]

sutama = sum(dutama)
ssamp = sum(dsamp)

print(abs(sutama-ssamp))

# versi 2
# def fun(M):
#     n = len(M)
#     u = sum([M[i][i] for i in range(n)])
#     v = sum([M[i][n-i-1] for i in range(n)])
#     return abs(u-v)

# n = int(input())
# m = []
# for i in range(n):
#     x = list(map(int, input().split()))
#     m.append(x)

# print(fun(m))