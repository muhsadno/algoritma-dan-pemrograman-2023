# https://www.hackerrank.com/challenges/mini-max-sum/problem
x = list(map(int, input().split()))
x.sort()
min4 = x[:4]
max4 = x[1:]
print(sum(min4), sum(max4))