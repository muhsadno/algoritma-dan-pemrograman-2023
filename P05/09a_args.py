def tambah2(a, b):
    return a+b

def tambah3(a, b, c):
    return a+b+c

def tambah(*args):
    print(args)
    print(args[0])
    return sum(args)

# print(tambah2(1,2))
# print(tambah3(1,2,3))
print(tambah(1, 2, 3, 4, 5, 10))