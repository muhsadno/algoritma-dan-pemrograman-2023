print("Contoh 1")
for _ in range(4):
    print('Hello')

print("\nContoh 2")
for i in range(1,5):
    print(i)

print("\nContoh 2a")
for i in range(1,5):
    print(i, end=' ')

print("\n\nContoh 3")
for i in range(1,10,2):
    print(i)

print("\nContoh 4")
for j in range(10,1,-2):
    print(j)