def faktorial(n):
    if n == 0 or n == 1:
        return 1 #rekursi berhenti
    else:
        return n * faktorial(n-1) #rekursi
    
n = 4
hasil = faktorial(n)
print(f"{n}! = {hasil}")

