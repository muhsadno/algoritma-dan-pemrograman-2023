#function tanpa argumen
def hello():
    print("Hello World")

#function dengan argumen
def hello2(nama):
    print(f"Hello {nama}")

hello()

hello2("budi") #tanpa keyword
hello2(nama='ani') #dengan keyword