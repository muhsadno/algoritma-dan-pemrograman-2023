# https://www.hackerrank.com/challenges/simple-array-sum/problem?isFullScreen=true
_ = int(input())
x = input().split()
y = [int(u) for u in x]
print(sum(y))