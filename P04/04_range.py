x = list(range(5)) # (0, 1, 2, 3, 4)
print(f'x = {x}')

y = list(range(2,6)) #dimulai dari 2 hingga sebelum 6
print(f'y = {y}')

z = list(range(0, 10, 2)) #dimulai dari 0 hingga sebelum 10 dan +2 untuk anggota selanjutnya
print(f'z = {z}')

w = list(range(5, 1, -1)) #dimulai dari 5 hingga sebelum 1 dan -1 untuk anggota selanjutnya
print(f'w = {w}')