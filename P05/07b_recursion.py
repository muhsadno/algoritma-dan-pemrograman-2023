'''
fpb(a,b), a dan b keduanya non-negatif (>=0) dan keduanya tidak nol sekaligus
fpb(a,b) dapat ditentukan dengan algoritma Euklid
            fpb(a,b) = fpb(b, a mod b)
gunakan sifat fpb(x, 0) = x untuk setiap bilangan asli x
'''

def fpb(a,b):
    if b == 0:
        return a
    else:
        return fpb(b, a % b)
    
a = 12
b = 15
print(fpb(a,b))