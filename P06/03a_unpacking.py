data = [x for x in range(1,20) if x%2 != 0]
print(data)

#menampilkan isi data secara horizontal
for x in data:
    print(x, end=' ')
print()

#dengan unpacking list
print(*data)