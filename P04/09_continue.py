#menghitung banyak bilangan bukan kelipatan a atau b pada rentang [1, n] (inklusif)
n = 20
a, b = 3, 2
bil = []
count = 0
for x in range(1,n+1):
    if x%a == 0 or x%b == 0:
        continue
    else:
        count +=1
        bil.append(x)

print(bil)
print(f"count = {count}")