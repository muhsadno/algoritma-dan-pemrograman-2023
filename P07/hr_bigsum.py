# https://www.hackerrank.com/challenges/a-very-big-sum/problem?isFullScreen=true
n = int(input())
a = list(map(int, input().split()))
print(sum(a))