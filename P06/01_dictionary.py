print("Contoh 1")
data = { 'nama' : 'iman', 'prodi': 'sistem informasi'}
print(data['nama'])
print(data.get('prodi')) #cara lain akses item dictionary menggunakan get

print("\nContoh 2")
X = {0 : 'Makassar', 'kampus': 'Unhas'}
print(X[0])

print("\nContoh 3")
Y = {'prima4' : [2, 3, 5, 7], 
     'kamus': {'go': 'pergi', 'one': 1},
     }
print(Y['prima4'][0])
print(Y['kamus']['one'])

print("\nContoh 4")
iman = {'algoritma' : 10, 'bahasa': 9.8, 'algoritma': 8.8}
print(iman)

