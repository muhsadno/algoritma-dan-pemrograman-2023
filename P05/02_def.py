def haloKampus(nama, kampus):
    print(f"Halo saya {nama}, berasal dari {kampus}")

haloKampus('Iman', 'Unhas') #positional arguments
haloKampus(nama='Usman', kampus='UNM') #keyword arguments
haloKampus(kampus='UMI', nama='Junaid')